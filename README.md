# demoscene-no-brasil

LaTeX source code from article: _Demoscene in Brazil: A Quasi-Systematic
Mapping Study_.

## Latex build dependencies

    sudo apt install texlive-publishers

## Build pdf

    make

## LICENSE

GNU GPL v3

## AUTHOR

Joenio Marques da Costa
